﻿
// QuickCMDDlg.h: 头文件
//

#pragma once

// CQuickCMDDlg 对话框
class CQuickCMDDlg : public CDialogEx
{
// 构造
public:
	CQuickCMDDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_QUICKCMD_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	CListBox* m_pListBox;		// 列表控件
	int m_nScrWidth;			// 屏幕宽度
	int m_nScrHeight;			// 屏幕高度
	CString m_sAppDataPath;		// AppData 路径
	CString m_sConfigFile;		// 配置文件路径

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
private:
	void ResizeMain();
	int RefreshList();
	static BOOL CALLBACK EnumWindowProc(HWND hWnd, LPARAM lParam);
public:
	afx_msg void OnSelchangeList();
	afx_msg void OnDblclkList();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
private:
	void ResetPos(bool bResetSize);
public:
	afx_msg void OnActivateApp(BOOL bActive, DWORD dwThreadID);
	afx_msg void OnClose();
	CString GetAppDataPath();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	void DeleteSels();
};
